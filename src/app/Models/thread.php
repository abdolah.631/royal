<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class thread extends Model
{
    use HasFactory;

    public  function channel(){
        return $this->belongsTo(channel::class);
    }

    public  function user(){
        return $this->belongsTo(User::class);
    }

    public  function answer(){
        return $this->hasMany(answer::class);
    }
}
