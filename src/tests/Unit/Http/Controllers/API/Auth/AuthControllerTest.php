<?php

namespace Tests\Unit\Http\Controllers\API\Auth;

use App\Http\Controllers\API\Auth\AuthController;

//use PHPUnit\Framework\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_register_should_be_validate()
    {
        $res = $this->postJson('/api/auth/register');

        $res->assertStatus(422);
    }

    public function test_new_user_can_register()
    {
        $res = $this->postJson('/api/auth/register', [
            'name' => 'abdolah',
            'email' => 'abdolah@gmail.com',
            'password' => '123456',
        ]);

        $res->assertStatus(201);
    }

    public function test_login_should_be_validate()
    {
        $res = $this->postJson(route('auth.login'));
        $res->assertStatus(422);
    }

    public function test_user_can_login()
    {
        $res = $this->postJson(route('auth.login', [
            'email' => 'abdolah@gmail.com',
            'password' => '123456',

        ]));
        $res->assertStatus(200);
    }

    public function test_user_logged_in_can_logout()
    {
        $user = factory(User::class)->create();
        $res = $this->actingAs($user)->postJson(route('auth.logout'));
        $res->assertStatus(200);
    }

}
